### Objectifs
Comprendre et progresser dans la découverte du langage **Python**.
### Pré-requis à cette activité
Connaitre les commandes de base et avoir accés à un _interprétateur_ **Python**
### Durée de l'activité
2 séances de 2 heures
### Exercices cibles
Des Exercices de programmation de figures de plus en plus complexes, chaque niveau demandant l'aquisition du niveau précédent
### Description du déroulement de l'activité
* Enoncé de l'Exercice
* Réalisation
* Corrigé
### Anticipation des difficultés des élèves
* La prise en main de l'_interprétateur_
* **Les erreurs de syntaxes**
* La compréhension du fil logique
### Gestion de l'hétérogénéïté
Possibilité de travailler en groupe sur les derniers Exercices
